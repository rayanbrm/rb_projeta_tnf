# Rayan Barama - Projet A

## Built with

![Ansible](https://img.shields.io/static/v1?style=for-the-badge&message=Ansible&color=EE0000&logo=Ansible&logoColor=FFFFFF&label=) ![Ubuntu](https://img.shields.io/static/v1?style=for-the-badge&message=Ubuntu&color=E95420&logo=Ubuntu&logoColor=FFFFFF&label=) ![GNU Bash](https://img.shields.io/static/v1?style=for-the-badge&message=GNU+Bash&color=4EAA25&logo=GNU+Bash&logoColor=FFFFFF&label=) ![Proxmox](https://img.shields.io/static/v1?style=for-the-badge&message=Proxmox&color=E57000&logo=Proxmox&logoColor=FFFFFF&label=) ![Apache](https://img.shields.io/static/v1?style=for-the-badge&message=Apache&color=D22128&logo=Apache&logoColor=FFFFFF&label=) ![MariaDB](https://img.shields.io/static/v1?style=for-the-badge&message=MariaDB&color=003545&logo=MariaDB&logoColor=FFFFFF&label=) ![NGINX](https://img.shields.io/static/v1?style=for-the-badge&message=NGINX&color=009639&logo=NGINX&logoColor=FFFFFF&label=)

## Context

Wewebeo is a learning project in which I put into practice the elements I learn in my one year DevOps training at ITS and The Nuum Factory. This project is divided in three parts : 

- Automation of infrastructure and application deployments
- Pipeline integration and containerization 
- Deployment on Cloud

For the time being, I am at the first stage. I decided to focus my work around Ansible.

## Description

Wewebeo is a deployment tool for sites based on PHP and Mariadb on Apache and Mariadb servers. 
At launch, the user enters the necessary information for deployment through a bash script. These informations are then used as variables to run the playbook.  

## Visuals

This visuals represent the current state of the project. It will evolve at each major advance. 

![Project_scheme](https://www.dropbox.com/s/kykcjyco70xxe3s/Sans-titre-2022-09-30-0956.png?raw=1)

## Requirements

Below, you can find the configuration I worked on. It is possible that my project works on other versions but I strongly advise you to use those.

| Plateform | Version |
| ------ | ------ |
| Ubuntu       |    20.04    |
| Ansible     |   2.9.6     |

Please note that you need to change the Ansible host file with your own servers ip !
  
## Installation

First you need to make sure your managed nodes are accessible through ssh key. Follow this tutorials for more details : https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804

Clone the repository:
```
git clone https://gitlab.com/rayanbrm/rb_projeta_tnf.git
```
Move to the cloned directory:
```
cd rb_project_tnf
```
Enter correct data in inventory/host_vars/ files (ip, port, username)
```
cd inventory/host_vars/
```
Create an ansible-vault which contains your sudo password in inventory/group_vars/all/
```
cd inventory/group_vars/all/; ansible-vault create credentials.yaml
```
Run the script:
```
./deploy.sh
```


## Usage

Here is a short demo of the script (clicking on start will open an asciinema.org new tab)

<a href="https://asciinema.org/a/gOrEQ7OtxoTAk6uxuLa2ZQHc6?autoplay=1" target="_blank"><img src="https://asciinema.org/a/gOrEQ7OtxoTAk6uxuLa2ZQHc6.svg" /></a>

## Roadmap

- [ ] Add a reverse-proxy server.
- [ ] Add a DNS server.
- [ ] Add a Jenkins pipeline that update site when a new commit is made on the main branch of the website repo.
- [ ] Add Docker containers for each site.
- [ ] Work on security  

## Project status

This project is in developement. 

## Support 

If you try my project and encounter an error or if you have tips for improvement, do not hesitate to contact me on my LinkedIn



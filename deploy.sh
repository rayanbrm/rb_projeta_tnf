#!/bin/bash

function quitWewebeo(){
        echo ""
        gum style --border normal --margin "1" --padding "1 2" --border-foreground 212 "Thanks for using $(gum style --foreground 212 "Wewebeo") ! See you later."
        sleep 1
        exit
}

function getProjectName(){

    while true
        do
            echo -e "Please enter a $(gum style --foreground 212 "name for your project"). This name will be used as directory name in /var/www/"
            APACHE_HOST_DIR=$(gum input --placeholder "Enter the name of your project ?")
            if [[ $APACHE_HOST_DIR == "quit" ]] ; then
                quitWewebeo
            elif [[ -z $APACHE_HOST_DIR ]]; then
                    echo -e "> Can't be empty"
                    sleep 1
                    echo ""
            else
                $(ssh -q afithae@nodeweb test -d /var/www/$APACHE_HOST_DIR)
                if [[ $? -eq 0 ]]; then
                    echo -e "> Ooops! A website with the name $(gum style --foreground 212 "$APACHE_HOST_DIR") already exist on our server."
                    sleep 1
                    echo ""
                else
                    echo -e "> Great! We are happy to help you deploy $(gum style --foreground 212 "$APACHE_HOST_DIR") on our server."
                    sleep 1
                    echo ""
                    break
                fi
            fi
        done


}

function getDbName(){

        while true 
        do 
            echo -e "Please enter a $(gum style --foreground 212 "name for the database"). It must match the one on mysqlconnect()"
		    DB_NAME=$(gum input --placeholder "Enter the a name for the database ?")
            if [[ $DB_NAME == "quit" ]] ; then
                quitWewebeo
            elif [[ -z $DB_NAME ]]; then
                    echo -e "> Can't be empty"
                    sleep 1
                    echo ""
            else
                $(ssh afithae@nodedb test -d /var/lib/mysql/$DB_NAME)
                if [[ $? -eq 0 ]]; then
                    echo -e "> Ooops! A database with the name $(gum style --foreground 212 "$DB_NAME") already exist on our server."
                    sleep 1
                    echo ""
                else
                    echo -e "> $(gum style --foreground 212 "$DB_NAME") will be deployed on our server."
                    sleep 1
                    echo ""
                    break  
                fi
            fi
        done

}

function getDbUsername(){

        echo -e "Please enter an $(gum style --foreground 212 "username for the database access")."
		DB_USER_NAME=$(gum input --placeholder "Enter the username for the database access ?")
        if [[ $DB_USER_NAME == "quit" ]] ; then
                quitWewebeo
        elif [[ -z $DB_USER_NAME ]]; then
        echo -e "> Can't be empty"
        sleep 1
        echo ""
        else
            echo -e "> $(gum style --foreground 212 "$DB_USER_NAME") will be granted access to the database $(gum style --foreground 212 "${APACHE_HOST_DIR}db") "
            sleep 1
            echo ""
        fi

}

function getDbPassword(){

        echo -e "Please enter a $(gum style --foreground 212 "password") for the database user $(gum style --foreground 212 "$DB_USER_NAME")."
		DB_USER_PASSWORD=$(gum input --placeholder "Enter the password for $DB_USER_NAME ?")
        if [[ $DB_USER_PASSWORD == "quit" ]] ; then
                quitWewebeo
        elif [[ -z $DB_USER_PASSWORD ]]; then
            echo -e "> Can't be empty"
            sleep 1
            echo ""
        else
            echo -e "> It is well stored !"
            sleep 1
            echo ""
        fi

}

function getGitUrl(){
        
        echo -e "Please enter the $(gum style --foreground 212 "git url") of your project $(gum style --foreground 212 "$APACHE_HOST_DIR")."
		GIT_URL=$(gum input --placeholder "Enter the git url for $APACHE_HOST_DIR ?")
        if [[ -n $(echo $GIT_URL | grep .git$) ]]; then
            GIT_URL=${GIT_URL%.git}
        fi
        if [[ $GIT_URL == "quit" ]] ; then
            quitWewebeo
        elif [[ -z $GIT_URL ]]; then
            echo -e "> Can't be empty"
            sleep 1
            echo ""
        else
            echo -e "> We can find your projet at $(gum style --foreground 212 "${GIT_URL}.git") !"
            sleep 1
            echo ""
        fi

}

function getDbScript(){
        
        echo -e "Please enter the $(gum style --foreground 212 "name of your database insert script"). It must be in your git directory."
		DB_SCRIPT=$(gum input --placeholder "Enter the name of your database insert script")
        if [[ -z $(echo $DB_SCRIPT | grep .sql$) ]]; then
            DB_SCRIPT="$DB_SCRIPT.sql"
        fi
        if [[ $DB_SCRIPT == "quit" ]] ; then
                quitWewebeo
        elif [[ -z $DB_SCRIPT ]]; then
            echo -e "> Can't be empty"
            sleep 1
            echo ""
        else
            echo -e "> Got it ! We will use $(gum style --foreground 212 "$DB_SCRIPT") to insert data into the database !"
            sleep 1
            echo ""
        fi
}

function selectPort(){

        AVAILABLE_PORTS=()
        for ((i = 0 ; i < 5 ; i++)); do
            while true
            do
                POSSIBLE_PORT=$((8000 + $RANDOM % 1000))
                if  [[ -z $(lsof -i:$POSSIBLE_PORT) ]]; then
                    AVAILABLE_PORTS+=( $POSSIBLE_PORT )
                    break
                fi
            done
        done

}

function getPort(){

        selectPort
        echo -e "On which $(gum style --foreground 212 "port") do you want to deploy your website !"
        PORT=$(gum choose "${AVAILABLE_PORTS[0]}" "${AVAILABLE_PORTS[1]}" "${AVAILABLE_PORTS[2]}" "${AVAILABLE_PORTS[3]}" "${AVAILABLE_PORTS[4]}")
        echo -e "> We will deploy $APACHE_HOST_DIR on port $(gum style --foreground 212 "$PORT") !"
}

function dataCapture(){

        getProjectName
        getDbName
        getDbUsername
        getDbPassword
        getGitUrl
        getDbScript
        getPort
}

function verifyData(){

        echo -e "Your project name is $(gum style --foreground 212 "$APACHE_HOST_DIR")"
        echo -e "Your database user name is $DB_USER_NAME (must match the one in your mysqlconnect())"
        echo -e "Your git url is $(gum style --foreground 212 "${GIT_URL}.git")"
        echo -e "Your database script (must be in your git repo) is $(gum style --foreground 212 "$DB_SCRIPT")"
        echo -e "Your site will be deployed on port (must be in your git repo) is $(gum style --foreground 212 "$PORT")"

}

function runansible(){

        ansible-playbook wewebeo.yaml -e "db_name=$DB_NAME db_user_name=$DB_USER_NAME db_user_password=$DB_USER_PASSWORD apache_port=$PORT apache_host_dir=$APACHE_HOST_DIR apache_conf_file=${APACHE_HOST_DIR}.conf git_url=$GIT_URL db_script=$DB_SCRIPT" > /tmp/pipeline.txt &
        pid=$!
        while kill -0 $pid 2> /dev/null; do
            gum spin --title "Deploying $(gum style --foreground 212 "$APACHE_HOST_DIR") on our server..." -- sleep 10;
        done

}

function deployApp(){

    echo -e "Let's go for the $(gum style --foreground 212 "deployment of a new app")."
	sleep 1
    echo ""

    while true
    do
        while true
        do
            dataCapture
            echo ""
            gum spin --title "Reading all your $(gum style --foreground 212 "answers")..." -- sleep 3;clear
            verifyData
            $(gum confirm "Is it correct ?")
            if [[ $? -eq 0 ]];
            then
                clear
                runansible
                break
            else
                clear
            fi
        done
        if [[ -z $(grep -Po "(?<=failed=)." /tmp/pipeline.txt | grep 1)  ]]; 
        then
            echo -e "$(gum style --foreground 212 "$APACHE_HOST_DIR") is accessible at the address $(gum style --foreground 212 "http://192.168.1.196:$PORT")"
            sleep 10
            echo ""
            echo -e "Merci d'avoir utilisé notre service !"
            exit
        else
            echo -e "Ooopss... An $(gum style --foreground 212 "error occured")... Please enter your informations again !"
            sleep 3
            ansible nodeweb -m ansible.builtin.file -a "path=/var/www/$APACHE_HOST_DIR state=absent" -b > /dev/null
            ansible nodedb -m mysql_db -a "name=$DB_NAME state=absent login_unix_socket=/run/mysqld/mysqld.sock" -b > /dev/null
            sleep 2
            echo ""
        fi
    done
}

clear
while true
do
gum style --border normal --margin "1" --padding "1 2" --border-foreground 212 " Hello, there! Welcome to $(gum style --foreground 212 'Wewebeo')."

echo "What would you like to do today ?"
echo ""

    MENU=$(gum choose "Deploy a new app" "Update an app" "Quit")
	if [[ $MENU == "Deploy a new app" ]]; then
        deployApp
    elif [[ $MENU == "Quit" ]]; then
        quitWewebeo
    else
        clear
        echo "Coming soon !"
        clear
        sleep 1
    fi
done